defmodule CardsTest do
  use ExUnit.Case
  doctest Cards

  test "creates a deck of length 20 cards" do
    assert length(Cards.create_deck) == 20
  end
end
